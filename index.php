<?php
 
?>
<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Xml to Json Converter</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" >
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
	 <h2>Xml to Json Converter</h2>
  <div class="panel panel-default">
    <div class="panel-body">
	<form name="frmxml" action="json-download.php" enctype="multipart/form-data" method="POST" >  
     
	    	<div class="form-group">
            <label for="file"> Choose XML file here</label>
 
            <input name="xmlfile" type="file" id="file" class="form-control-file" required="required"  >	          
            </div>  
         
             <button type="submit"  class="btn btn-primary btn-md  ">Submit</button> 
               
	  </form> 

	
    </div>
       	<div class="clearfix" ></div>
       	<br clear="all">
   
  </div>
	 

</div>
</body>

</html>